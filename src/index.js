import ReactDOM from 'react-dom'
import {Provider} from 'react-redux'
import App from './components/app'
import {store} from './resources/store'
import React from "react";

import * as serviceWorker from './serviceWorker';

ReactDOM.render(
        <Provider store={store}>
            <App />
        </Provider>,
    document.getElementById('root')
)

serviceWorker.unregister();
