import {commentConstants} from "../resources/constants/comment.constants"

export function comments(state = {}, action) {
  switch (action.type) {
    case commentConstants.COMMENT_REGISTER_REQUEST:
      return {
        savingC: true
      }
    case commentConstants.COMMENT_REGISTER_SUCCESS:
      console.log('new comment in the post list: ', action.posts[0].commentList)
      return {
        comment: action.comment,
        post: action.post,
        posts: action.posts
      }
    case commentConstants.COMMENT_REGISTER_FAILURE:
      return {
        error: action.error
      }
    default:
      return state
  }
}