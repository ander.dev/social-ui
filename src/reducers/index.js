import {combineReducers} from 'redux'

import {authentication} from './authentication.reducer'
import {registration} from './registration.reducer'
import {users} from './users.reducer'
import {posts} from './posts.reducer'

const rootReducer = combineReducers({
    authentication,
    registration,
    users,
    posts
})

export default rootReducer
